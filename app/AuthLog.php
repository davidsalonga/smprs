<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthLog extends Model
{
    protected $fillable = [
        'authenticatable_type', 'autheticatable_id', 'login_at', 'logout_at',
    ];

    // Table name
    protected $table = 'authentication_log';
    public $primaryKey = 'id';
    public $searchTerm = 'created_at';
    public $timestamps = true;  


}
