<?php

namespace App\Exports;

use App\Art;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ArtJobExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Art::all();
    }

    public function headings(): array
    {
        return [
            'ID',
            'BRANCH',
            'DATE_REQUEST',
            'REQUEST_BY',
            'DATE_REQUIRED',
            'PROJECT_NAME',
            'DEPARTMENT',
            'PURPOSE',
            'USAGE',
            'MOBILENO',
            'REQUIREMENTS',
            'DETAILS',
            'SIZE',
            'QUANTITY',
            'OTHERS',
            'STATUS',
            'COMMENT',
            'CREATED_AT',
            'UPDATED_AT',


        ];
    }
}
