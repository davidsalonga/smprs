<?php

namespace App\Exports;

use App\Proposal;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RequestExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Proposal::all();
    }

    public function headings(): array
    {
        return [
            
            'ID',
            'BRANCH',
            'DATE REQUESTED',
            'REQUESTED BY',
            'MOBILE NO.',
            'SCHEDULE POST',
            'DETAILS',
            'EVENT TITLE',
            'EVENT VENUE',
            'EVENT DATE',
            'START TIME',
            'END TIME',
            'SOCIAL MEDIA LINK',
            'CAPTION',
            'IMAGE/VIDEO LINK',
            'FAN PAGE LINK',
            'STATUS[1 - Posted, 2 - For Revision, 3 - User Updated, 4 - Pending]',
            'COMMENTS',
            'CREATED AT',
            'UPDATED AT',

        ];
    }
}
