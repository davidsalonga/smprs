<?php

namespace App\Exports;

use App\AuthLog;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;


class AuthLogExport implements FromCollection, ShouldAutoSize, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return AuthLog::all('authenticatable_id', 'ip_address', 'login_at', 'logout_at');
    }

    public function headings(): array
    {
        return [
            
            'AUTHETICATABLE ID',
            'IP ADDRESS',
            'LOGIN AT',
            'LOGOUT AT',
        ];
    }
}
