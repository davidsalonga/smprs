<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Proposal;
use App\Art;



class ProposalController extends Controller
{


      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'show', 'store', 'edit', 'update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   
    public function index()
    {
        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending
        $approved = DB::table('proposals')->where('status', 'like', '1')->count();
        $forRevision = DB::table('proposals')->where('status', 'like', '2')->count();
        $revised     = DB::table('proposals')->where('status', 'like', '3')->count();
        $pending     = DB::table('proposals')->where('status', 'like', '4')->count();

        $proposals   = Proposal::orderBy('updated_at', 'desc')->where('status', 'like', '0')
                                                              ->orwhere('status', 'like', '2')
                                                              ->orWhere('status', 'like' , '3')
                                                              ->orWhere('status', 'like' , '4')
                                                              ->paginate(10);


        return view('proposals.index')->with('proposals',     $proposals)
                                      ->with('forRevision', $forRevision)
                                      ->with('revised',         $revised)
                                      ->with('pending',         $pending)
                                      ->with('approved',         $approved);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('proposals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'branch' => 'required',
            'date_request' => 'required',
            'request_by' => 'required',
            'mobileno' => 'required|numeric|min:11|',
            'schedule_post' => 'required',
            'details' => 'required',
            'eventTitle' => 'required',
            'eventVenue' => 'required',
            'eventDate' => 'required',
            'startTime' => 'required',
            'endTime' => 'required|after:startTime',
            'social' => 'required',
            'postText' => 'required',
            'imgvidUrl' => 'required',
            'fanpage' => 'required',
        ]);
       
        $proposal = new Proposal;
        $proposal->branch        = $request->input('branch');
        $proposal->date_request  = $request->input('date_request');
        $proposal->request_by    = $request->input('request_by');
        $proposal->mobileno      = $request->input('mobileno');
        $proposal->schedule_post = $request->input('schedule_post');
        $checkBox1               = implode(',', $request->input('details'));
        $proposal->details       = $checkBox1;
        $proposal->eventTitle    = $request->input('eventTitle');
        $proposal->eventVenue    = $request->input('eventVenue');
        $proposal->eventDate     = $request->input('eventDate');
        $proposal->startTime     = $request->input('startTime');
        $proposal->endTime       = $request->input('endTime');
        $checkBox2               = implode(',', $request->input('social'));
        $proposal->social        = $checkBox2;
        $proposal->postText      = $request->input('postText');
        $proposal->imgvidUrl     = $request->input('imgvidUrl');
        $proposal->fanpage       = $request->input('fanpage');
        $proposal->comment       = $request->input('comment');
        // $proposal->user_id = auth()->user()->id;
        $proposal->save();
        $reference = $timestamp  = now();
        $mobileno = $proposal->mobileno;
        $eventName = $proposal->eventTitle;
        // sends notification on user mobile
        $this->notifyUser($mobileno, $reference, $eventName);
        
        
        return redirect('/proposals/create')->with('success', 'Proposal Submitted')
                                            ->with('reference', $reference);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $proposal = Proposal::find($id);
        // $this->notifyUser($proposal->mobileno, $proposal->created_at, $proposal->eventTitle);
        return view('proposals.show')->with('proposal', $proposal);
    }



    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proposal = Proposal::find($id);
        return view('proposals.update')->with('proposal', $proposal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'branch' => 'required',
            'date_request' => 'required',
            'request_by' => 'required',
            'mobileno' => 'required|numeric|min:11|',
            'schedule_post' => 'required',
            'details' => 'required',
            'eventTitle' => 'required',
            'eventVenue' => 'required',
            'eventDate' => 'required',
            'startTime' => 'required',
            'endTime' => 'required|after:startTime',
            'social' => 'required',
            'postText' => 'required',
            'imgvidUrl' => 'required',
            'fanpage' => 'required',
        ]);

        $proposal                   = Proposal::find($id);
        $proposal->branch           = $request->input('branch');
        $proposal->date_request     = $request->input('date_request');
        $proposal->request_by       = $request->input('request_by');
        $proposal->mobileno         = $request->input('mobileno');
        $proposal->schedule_post    = $request->input('schedule_post');
        $checkBox1                  = implode(',', $request->input('details'));
        $proposal->details          = $checkBox1;
        $proposal->eventTitle       = $request->input('eventTitle');
        $proposal->eventVenue       = $request->input('eventVenue');
        $proposal->eventDate        = $request->input('eventDate');
        $proposal->startTime        = $request->input('startTime');
        $proposal->endTime          = $request->input('endTime');
        // $proposal->social = $request->input('social'); //checkbox item
        $checkBox2                  = implode(',', $request->input('social'));
        $proposal->social           = $checkBox2;
        $proposal->postText         = $request->input('postText');
        $proposal->imgvidUrl        = $request->input('imgvidUrl');
        $proposal->fanpage          = $request->input('fanpage');
        $proposal->comment          = $request->input('comment');
        // $proposal->user_id = auth()->user()->id;

        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending

        $updateId = Proposal::find($id);
        $updateId->status = "3";
        $updateId->save();
        $proposal->save();

        return redirect()->action('ProposalController@show', ['id' => $id])->with('proposalUpdated', 'Proposal Updated');
        // return redirect()->back()->with('proposalUpdated', 'Proposal Updated')->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function notifyUser($mobileno, $reference, $eventName){

        $ch = curl_init();
        $parameters = array(
            'apikey' => '1560f9b8c8e34d26cd1ce1dbf66194ef', //Your API KEY
            'number' => $mobileno,
            'message' => " SMX Marketing Communication\n\n Event Title: \n ".$eventName."\n\n Your reference # is: \n ".$reference,
            'sendername' => 'SEMAPHORE'
        );

        curl_setopt( $ch, CURLOPT_URL,'https://semaphore.co/api/v4/messages' );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        //Send the parameters set above with the request
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

        // Receive response from server
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close ($ch);
        return redirect()->back();
   }

}
