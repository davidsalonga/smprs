<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Proposal;
use App\User;
use App\Art;
use Auth;
use App\Exports\RequestExport;
use Maatwebsite\Excel\Facades\Excel;

class PagesController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // blocks user from entering admin side except index, search and update.
        $this->middleware('auth', ['except' => ['index', 'search', 'update']]);
    }
    
    
    public function index(){
        return view('proposals.search');
    }

    public function search(Request $request){

        $this->validate($request, [
            'searchPost' => 'required',
        ]);

        $userInput  = $request->input('searchPost');
        $created_at = DB::table('proposals')->where('created_at' , 'like' , '%'.$userInput.'%')->get();   
        $created_at2 = DB::table('artjobrequest')->where('created_at' , 'like' , '%'.$userInput.'%')->get();   
        // $created_at = DB::table('artjobrequest')->where('created_at' , 'like' , '%'.$userInput.'%')->get(); 
        return view('proposals.view')->with('proposals', $created_at)
                                     ->with('artjob', $created_at2);
        
        
        // return $created;
    }

    public function searchArt(Request $request){

        $this->validate($request, [
            'searchArt' => 'required',
        ]);

        $userInput  = $request->input('searchArt');
        $created_at = DB::table('artjobrequest')->where('created_at' , 'like' , '%'.$userInput.'%')->get();   
        return view('artjob.view')->with('artjob', $created_at);
        
        
        // return $created;
    }


    public function update(Request $request){
        
        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending
        

        $id = $request->input('proposalId');
        $updateId = Proposal::find($id);
        $updateId->status = "1";
        $updateId->comment = "";
        $updateId->save();

        // SMS Notification
        $msg = "\n Event title: ".$updateId->eventTitle."\n\n Your request has been APPROVED!";
        $this->notifyUser($updateId->mobileno, $msg);
    

        return  redirect()->back()->with('Updatesuccess', 'Proposal Approved');
    }


    public function updateArt(Request $request){
        
        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending
        

        $id = $request->input('proposalId');
        $updateId = Art::find($id);
        $updateId->status = "1";
        $updateId->comment = "";
        $updateId->save();

        // SMS Notification
        $msg = "\n Art Job Request - Project Name: ".$updateId->project_name."\n\n Your request has been APPROVED!";
        $this->notifyUser($updateId->mobileno, $msg);
    

        return  redirect()->back()->with('Updatesuccess', 'Art Job Approved');
    }

    public function viewApproved(){
        $approved = DB::table('proposals')->where('status', 'like', '1')->count();
        $forRevision = DB::table('proposals')->where('status', 'like', '2')->count();
        $revised     = DB::table('proposals')->where('status', 'like', '3')->count();
        $pending     = DB::table('proposals')->where('status', 'like', '4')->count();

        $proposals = Proposal::orderBy('created_at', 'desc')->where('status', 'like', '1')->paginate(5);
        

        return view('proposals.approved')->with('proposals',     $proposals)
                                      ->with('forRevision', $forRevision)
                                      ->with('revised',         $revised)
                                      ->with('pending',         $pending)
                                      ->with('approved',         $approved);
        //return view('proposals.approved')->with('proposals', $proposals)
        //                                  ->with('users', $user);
    }

    public function comment(Request $request){

        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending

        $this->validate($request, [
            'comment' => 'required',
        ]);
        
        $id = $request->input('id');
        $addComment = Proposal::find($id);
        $addComment->comment =  $request->input('comment');
        $addComment->status = '2';
        $addComment->save();
        // SMS notification
        $msg = "\n Event title: ".$addComment->eventTitle."\n\n Comment: \n".$addComment->comment;
        $this->notifyUser($addComment->mobileno, $msg);

        //Show the server response
       


        return redirect()->back()->with('commentAdded', 'Comment Added');
    }

    public function commentArt(Request $request){

        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending

        $this->validate($request, [
            'comment' => 'required',
        ]);
        
        $id = $request->input('id');
        $addComment = Art::find($id);
        $addComment->comment =  $request->input('comment');
        $addComment->status = '2';
        $addComment->save();
        // SMS notification
        $msg = "\n Art Job Request - Project Name: ".$addComment->project_name."\n\n Comment: \n".$addComment->comment;
        $this->notifyUser($addComment->mobileno, $msg);

        //Show the server response
       


        return redirect()->back()->with('commentAdded', 'Comment Added');
    }


    public function showForRevision(){

        $approved    = DB::table('proposals')->where('status', 'like', '1')->count();
        $forRevision = DB::table('proposals')->where('status', 'like', '2')->count();
        $report      = DB::table('proposals')->where('status', 'like', '3')->count();
        $pending     = DB::table('proposals')->where('status', 'like', '4')->count();

        $proposals = Proposal::orderBy('created_at', 'desc')->where('status', 'like', '2')->paginate(5);
        return view('status.forRevision')->with('proposals',     $proposals)
                                         ->with('approved',       $approved)
                                         ->with('revised',          $report)
                                         ->with('forRevision', $forRevision)
                                         ->with('pending',         $pending);
   }



    public function showRevised(){

         $approved     = DB::table('proposals')->where('status', 'like', '1')->count();
         $forRevision = DB::table('proposals')->where('status', 'like', '2')->count();
         $report      = DB::table('proposals')->where('status', 'like', '3')->count();
         $pending     = DB::table('proposals')->where('status', 'like', '4')->count();

        $proposals    = Proposal::orderBy('created_at',     'desc')
                                     ->where('status', 'like', '3')
                                     ->paginate(2);

        return view('status.revised')->with('proposals',     $proposals)
                                     ->with('approved',       $approved)
                                     ->with('revised',          $report)
                                     ->with('forRevision', $forRevision)
                                     ->with('pending',         $pending);
    }

    public function showPending(){

        $approved     = DB::table('proposals')->where('status', 'like', '1')->count();
        $forRevision = DB::table('proposals')->where('status', 'like', '2')->count();
        $report      = DB::table('proposals')->where('status', 'like', '3')->count();
        $pending     = DB::table('proposals')->where('status', 'like', '4')->count();

        $proposals   = Proposal::orderBy('created_at',     'desc')
                                    ->where('status', 'like', '4')
                                    ->paginate(                 5);

       return view('status.pending')->with('proposals', $proposals)
                                    ->with('approved', $approved)
                                    ->with('revised', $report)
                                    ->with('forRevision', $forRevision)
                                    ->with('pending', $pending);
   }

   
//start

public function showRevisedArt(){

    $approved     = DB::table('artjobrequest')->where('status', 'like', '1')->count();
    $forRevision = DB::table('artjobrequest')->where('status', 'like', '2')->count();
    $report      = DB::table('artjobrequest')->where('status', 'like', '3')->count();
    $pending     = DB::table('artjobrequest')->where('status', 'like', '4')->count();

   $proposals    = Art::orderBy('created_at',     'desc')
                                ->where('status', 'like', '3')
                                ->paginate(2);

   return view('status.artjob.revised')->with('artjob',     $proposals)
                                ->with('approved',       $approved)
                                ->with('revised',          $report)
                                ->with('forRevision', $forRevision)
                                ->with('pending',         $pending);
}

public function showPendingArt(){

    $approved     = DB::table('artjobrequest')->where('status', 'like', '1')->count();
    $forRevision = DB::table('artjobrequest')->where('status', 'like', '2')->count();
    $report      = DB::table('artjobrequest')->where('status', 'like', '3')->count();
    $pending     = DB::table('artjobrequest')->where('status', 'like', '4')->count();

    $proposals   = Art::orderBy('created_at',     'desc')
                                ->where('status', 'like', '4')
                                ->paginate(                 5);

   return view('status.artjob.pending')->with('artjob', $proposals)
                                ->with('approved', $approved)
                                ->with('revised', $report)
                                ->with('forRevision', $forRevision)
                                ->with('pending', $pending);
}

public function showForRevisionArt(){

    $approved    = DB::table('artjobrequest')->where('status', 'like', '1')->count();
    $forRevision = DB::table('artjobrequest')->where('status', 'like', '2')->count();
    $report      = DB::table('artjobrequest')->where('status', 'like', '3')->count();
    $pending     = DB::table('artjobrequest')->where('status', 'like', '4')->count();

    $proposals = Art::orderBy('created_at', 'desc')->where('status', 'like', '2')->paginate(5);
    return view('status.artjob.forRevision')->with('artjob',     $proposals)
                                     ->with('approved',       $approved)
                                     ->with('revised',          $report)
                                     ->with('forRevision', $forRevision)
                                     ->with('pending',         $pending);
}


public function viewApprovedArt(){
    $approved = DB::table('artjobrequest')->where('status', 'like', '1')->count();
    $forRevision = DB::table('artjobrequest')->where('status', 'like', '2')->count();
    $revised     = DB::table('artjobrequest')->where('status', 'like', '3')->count();
    $pending     = DB::table('artjobrequest')->where('status', 'like', '4')->count();

    $proposals = Art::orderBy('created_at', 'desc')->where('status', 'like', '1')->paginate(5);
    

    return view('status.artjob.approved')->with('artjob',     $proposals)
                                  ->with('forRevision', $forRevision)
                                  ->with('revised',         $revised)
                                  ->with('pending',         $pending)
                                  ->with('approved',         $approved);
    //return view('proposals.approved')->with('proposals', $proposals)
    //                                  ->with('users', $user);
}



   public function notifyUser($mobileno, $comment){

        $ch = curl_init();
        $parameters = array(
            'apikey' => '1560f9b8c8e34d26cd1ce1dbf66194ef', //Your API KEY
            'number' => $mobileno,
            'message' => " SMX Marketing Communication\n ".$comment,
            'sendername' => 'SEMAPHORE'
        );

        curl_setopt( $ch, CURLOPT_URL,'https://semaphore.co/api/v4/messages' );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        //Send the parameters set above with the request
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

        // Receive response from server
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close ($ch);
        return redirect()->back();
   }

   public function export() {
        return Excel::download(new RequestExport, 'request.xlsx');
    }

}

