<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Art;
use App\User;
use App\Exports\ArtJobExport;
use App\Exports\UserExport;
use App\Exports\AuthLogExport;
use Maatwebsite\Excel\Facades\Excel;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['create', 'show', 'store', 'edit', 'update']]);
    }

    public function index()
    {

        $approved    = DB::table('artjobrequest')->where('status', 'like', '1')->count();
        $forRevision = DB::table('artjobrequest')->where('status', 'like', '2')->count();
        $revised     = DB::table('artjobrequest')->where('status', 'like', '3')->count();
        $pending     = DB::table('artjobrequest')->where('status', 'like', '4')->count();

        $artjob   = Art::orderBy('updated_at', 'desc')        ->where('status', 'like', '0')
                                                              ->orwhere('status', 'like', '2')
                                                              ->orWhere('status', 'like' , '3')
                                                              ->orWhere('status', 'like' , '4')
                                                              ->paginate(10);

                                                              $showuser = auth()->user()->previousLoginAt();

        return view('artjob.index')   ->with('artjob',           $artjob)
                                      ->with('forRevision', $forRevision)
                                      ->with('revised',         $revised)
                                      ->with('pending',         $pending)
                                      ->with('approved',       $approved)
                                      ->with('user', $showuser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('artjob.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'branch' => 'required',
            'date_request' => 'required',
            'request_by' => 'required',
            'date_required' => 'required',
            'project_name' => 'required',
            'department' => 'required',
            'purpose' => 'required',
            'usage' => 'required',
            'mobileno' => 'required|numeric|min:11|',
            'requirements' => 'required', // checkbox
            'details' => 'required',
            'size' => 'required',
            'quantity' => 'required',
            
        ]);
       
        $proposal = new Art;
        $proposal->branch        = $request->input('branch');
        $proposal->date_request  = $request->input('date_request');
        $proposal->request_by    = $request->input('request_by');
        $proposal->date_required = $request->input('date_required');
        $proposal->project_name  = $request->input('project_name');
        $proposal->department    = $request->input('department');
        $proposal->purpose       = $request->input('purpose');
        $proposal->usage         = $request->input('usage');
        $proposal->mobileno      = $request->input('mobileno');
        $checkBox1               = implode(',', $request->input('requirements'));
        $proposal->requirements  = $checkBox1;
        $proposal->details       = $request->input('details');
        $proposal->size          = $request->input('size');
        $proposal->quantity      = $request->input('quantity');
        $proposal->others      = $request->input('others');
        

        // $proposal->user_id = auth()->user()->id;
        $proposal->save();
        $reference = $timestamp  = now();
        $mobileno = $proposal->mobileno;
        $artJob = $proposal->project_name;
        // sends notification on user mobile
        $this->notifyUser($mobileno, $reference, $artJob);
        
        
        return redirect('/artjob/create')->with('success', 'Request Submitted')
                                         ->with('reference', $reference); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artjob = Art::find($id);
        // $this->notifyUser($proposal->mobileno, $proposal->created_at, $proposal->eventTitle);
        return view('artjob.show')->with('artjob', $artjob);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artjob = Art::find($id);
        return view('artjob.update')->with('artjob', $artjob);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'branch' => 'required',
            'date_request' => 'required',
            'request_by' => 'required',
            'date_required' => 'required',
            'project_name' => 'required',
            'department' => 'required',
            'purpose' => 'required',
            'usage' => 'required',
            'mobileno' => 'required|numeric|min:11|',
            'requirements' => 'required', // checkbox
            'details' => 'required',
            'size' => 'required',
            'quantity' => 'required',
            
        ]);

        $proposal                = Art::find($id);
        $proposal->branch        = $request->input('branch');
        $proposal->date_request  = $request->input('date_request');
        $proposal->request_by    = $request->input('request_by');
        $proposal->date_required = $request->input('date_required');
        $proposal->project_name  = $request->input('project_name');
        $proposal->department    = $request->input('department');
        $proposal->purpose       = $request->input('purpose');
        $proposal->usage         = $request->input('usage');
        $proposal->mobileno      = $request->input('mobileno');
        $checkBox1               = implode(',', $request->input('requirements'));
        $proposal->requirements  = $checkBox1;
        $proposal->details       = $request->input('details');
        $proposal->size          = $request->input('size');
        $proposal->quantity      = $request->input('quantity');
        $proposal->others        = $request->input('others');

        // 1 - Posted
        // 2 - For Revision
        // 3 - User Updated
        // 4 - Pending

        $updateId = Art::find($id);
        $updateId->status = "3";
        $updateId->save();
        $proposal->save();

        return redirect()->action('ArtController@show', ['id' => $id])->with('proposalUpdated', 'Art Job Updated');
        


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    
    public function notifyUser($mobileno, $reference, $artJob){

        $ch = curl_init();
        $parameters = array(
            'apikey' => '1560f9b8c8e34d26cd1ce1dbf66194ef', //Your API KEY
            'number' => $mobileno,
            'message' => " SMX Marketing Communication\n\n Project Name: \n ".$artJob."\n\n Your reference # is: \n ".$reference,
            'sendername' => 'SEMAPHORE'
        );

        curl_setopt( $ch, CURLOPT_URL,'https://semaphore.co/api/v4/messages' );
        curl_setopt( $ch, CURLOPT_POST, 1 );

        //Send the parameters set above with the request
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

        // Receive response from server
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $output = curl_exec( $ch );
        curl_close ($ch);
        return redirect()->back();
   }

   public function export() {
    return Excel::download(new ArtJobExport, 'artJob.xlsx');
}

    public function exportAuth() {
        return Excel::download(new AuthLogExport, 'authlogs.xlsx');
    }

    public function exportUsers() {
        return Excel::download(new UserExport, 'users.xlsx');
    }

}
