<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model

{
   
    protected $fillable = [
        'branch', 'date_request'. 'request_by', 'schedule_post', 'details', 
        'eventTitle', 'eventVenue', 'eventDate', 'eventTime', 'social', 'postText',
        'imgvidUrl', 'fanpage', 'linkUrl',
    ];

    // Table name
    protected $table = 'proposals';
    public $primaryKey = 'id';
    public $searchTerm = 'created_at';
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
}



