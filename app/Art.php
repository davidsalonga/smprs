<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Art extends Model
{
    protected $fillable = [
        'branch', 'date_request', 'request_by', 'date_required',
        'project_name', 'department', 'purpose', 'usage',
        'mobileno', 'requirements', 'details', 'size',
        'quantity', 'paper', 'created_at', 'updated_at'
        
    ];

       // Table name
       protected $table = 'artjobrequest';
       public $primaryKey = 'id';
       public $searchTerm = 'created_at';
       public $timestamps = true;

       
}
