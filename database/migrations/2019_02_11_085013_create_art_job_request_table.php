<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtJobRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artjobrequest', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch');
            $table->string('date_request');
            $table->string('request_by');
            $table->string('date_required');
            $table->string('project_name');
            $table->string('department');
            $table->string('purpose'); 
            $table->string('usage');
            $table->string('mobileno');
            $table->string('requirements'); // checkbox
            $table->string('details'); 
            $table->string('size');
            $table->string('quantity');
            $table->string('others');
            $table->string('status');
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('art_job_request');
    }
}
