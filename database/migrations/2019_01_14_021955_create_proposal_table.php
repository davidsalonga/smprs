<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //working
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch');
            $table->string('date_request');
            $table->string('request_by');
            $table->string('mobileno');
            $table->string('schedule_post');
            $table->string('details');
            $table->string('eventTitle');
            $table->string('eventVenue')->nullable();
            $table->string('eventDate')->nullable();
            $table->string('startTime');
            $table->string('endTime');
            $table->string('social');
            $table->string('postText');
            $table->string('imgvidUrl');
            $table->string('fanpage');
            $table->integer('status')->default('4');
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
