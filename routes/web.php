<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//working

Route::get('/', function () {
    return view('home');
});



// Routes for Proposals
Route::get('/proposals/search', 'PagesController@index'); 
Route::get('/proposals/searched', 'PagesController@search');
Route::get('/proposals/approved', 'PagesController@viewApproved');
 
//status show social media
Route::get('/proposals/revised', 'PagesController@showRevised'); 
Route::get('/proposals/forRevision', 'PagesController@showForRevision'); 
Route::get('/proposals/pending', 'PagesController@showPending'); 

//status show artjob
Route::get('/artjob/revised', 'PagesController@showRevisedArt'); 
Route::get('/artjob/forRevision', 'PagesController@showForRevisionArt'); 
Route::get('/artjob/pending', 'PagesController@showPendingArt'); 
Route::get('/artjob/approved', 'PagesController@viewApprovedArt');

// approve social media
Route::post('approve', 'PagesController@update');
// approve art job
Route::post('approveArt', 'PagesController@updateArt');
// social media comment
Route::post('addComment', 'PagesController@comment');
// art job comment
Route::post('addCommentArt', 'PagesController@commentArt');

Route::get('export', 'PagesController@export');
Route::get('exportArt', 'ArtController@export');
Route::get('exportAuth', 'ArtController@exportAuth');
Route::get('exportUsers', 'ArtController@exportUsers');

Auth::routes();

// Routes for ArtJob

Route::get('/artjob/create', 'ArtController@create');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('proposals', 'ProposalController');
Route::resource('artjob', 'ArtController');
Route::resource('users', 'UserController');

Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

