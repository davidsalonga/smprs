@extends('layouts.app')
@section('title', 'View Art Job')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card animated fadeIn">
                <div class="card-header">Viewing Request  : <font color="red"><strong>{{$artjob->created_at}}</strong></font>
                    <font style="float:right;"> Status:

                        @auth
                        @if ($artjob->status == "1") 
                             <font color="green"><strong>APPROVED</strong></font> 
                        @elseif($artjob->status == "2")
                            <font color="#f9a825"><strong>For Revision</strong></font><br>
                                <form action="/approveArt" method="post">
                                    @csrf
                                        <input type="hidden" name="proposalId" value="{{$artjob->id}}">
                                        <button type="submit" class="btn btn-success" style="float:right;">Approve</button>
                                </form>
                         @elseif($artjob->status == "3")
                             <font color="#f9a825"><strong>Revised - to be checked</strong></font><br>
                                <form action="/approveArt" method="post">
                                    @csrf
                                      <input type="hidden" name="proposalId" value="{{$artjob->id}}">
                                        <button type="submit" class="btn btn-success"  style="float:right;">Approve</button>
                                </form>

                          @else($proposal->status == "0")
                                <font color="red"><strong>PENDING</strong></font>

                                  <form action="/approveArt" method="post">
                                     @csrf
                                         <input type="hidden" name="proposalId" value="{{$artjob->id}}">
                                         <button type="submit" class="btn btn-success"  style="float:right;">Approve</button>
                                  </form>
                        @endif    
                        @endauth
                        
                        @guest
                        @if ($artjob->status == "1") 
                           <font color="green"><strong>APPROVED</strong></font> 
                        @elseif($artjob->status == "2")
                        <font color="#f9a825"><strong>For Revision</strong></font><br>
                        @elseif($artjob->status == "3")
                        <font color="#f9a825"><strong>Revised - to be checked</strong></font><br>
                        @else
                        <font color="red"><strong>PENDING</strong></font>
                        @endif
                        <br>
                        <a href="/artjob/{{$artjob->id}}/edit" class="btn btn-default" style="float:right;">Edit Form</a> 
                       @endguest    
                        
                    </font>
                </div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            
                        </div>
                    @endif

                            
                            <div class="row">
                                    <div class="col-md-4">
                                            <label for="branch" class="form-group"><strong>Branch</strong></label><br>
                                            <label name="branch" class="form-control" placeholder="" value="" readonly>{{$artjob->branch}}</label>
                                            {{-- <input type="text" name="branch" id="" class="form-control" value="{{ old('branch') }}"> --}}
                                            {!! $errors->first('branch','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                    <label for="date_request" class="form-group"><strong>Date Requested</strong></label><br>
                                    <label class="form-control" readonly>{{$artjob->date_request}}</label>
                                    {!! $errors->first('date_request','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-4">
                                      <label for="date_request" class="form-group"><strong>Requested By</strong></label><br>
                                      <label class="form-control" readonly>{{$artjob->request_by}}</label>
                                      {!! $errors->first('request_by','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4">
                                <label for="mobileno" class="form-group"><strong>Mobile No. (11 Digits)</strong></label><br>
                               <label class="form-control" readonly>{{$artjob->mobileno}}</label>
                                {!! $errors->first('mobileno','<span class="help-block" style="color:red;">*:message</span>') !!}
                             
                            </div>
                            <div class="col-md-4">
                                    <label for="date_required" class="form-group"><strong>Date Required</strong></label><br>
                                    <label class="form-control" readonly>{{$artjob->date_required}}</label>
                                   {!! $errors->first('date_required','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            <br><br>
                            <br><br>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <label for="project_name" class="form-group"><strong>Project Name</strong></label><br>
                                  <label class="form-control" readonly>{{$artjob->project_name}}</label>
                                  {!! $errors->first('project_name','<span class="help-block" style="color:red;">*:message</span>') !!}
                                </div>
                            <div class="col-md-6">
                                    <label for="department" class="form-group"><strong>Department / Designation</strong></label><br>
                                    <label class="form-control" readonly>{{$artjob->department}}</label>
                                    {!! $errors->first('department','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                           </div>
                           <br>
                           <div class="row">
                                <div class="col-md-6">
                                  <label for="purpose" class="form-group"><strong>Purpose</strong></label><br>
                                  <textarea class="form-control" cols="5" rows="5" readonly>{{$artjob->purpose}}</textarea>
                                 {!! $errors->first('purpose','<span class="help-block" style="color:red;">*:message</span>') !!}
                                </div>
                            <div class="col-md-6">
                                    <label for="usage" class="form-group"><strong>Usage</strong></label><br>
                                    <textarea class="form-control" cols="10" rows="5" readonly>{{$artjob->usage}}</textarea>
                                {!! $errors->first('usage','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                           </div>
                            <br><hr>
                            <div class="row">
                                <div class="col-md-6">
                                        <label for="eventDetails" class="form-group"><strong>REQUIREMENTS</strong> </label>
                                        </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                                    <div class="checkbox">
                                                            @if($array = explode(",", $artjob->requirements))
                                             
                                                            @foreach ($array as $item)
                                                            &nbsp;<br><input type="checkbox" {{strpos($artjob->requirements, $item) !== false ? "checked" : ""}} disabled/>&nbsp;&nbsp;{{$item}}
                                                            @endforeach
                                                            
                                                            @endif 
                                                        </div>
                                                    <hr>
                                                </div>
                                                  
                                            
                                    </div>
                                    
                                    <div class="col-md-3">
                                            
                                                  <div class="checkbox">
                                                    <label for="requirements[]-11">
                                                      Others 
                                                      <textarea class="form-control" name="others" value="{{ old('others') }}" placeholder="Please specify" readonly >{{$artjob->others}}</textarea>
                                                    </label>
                                                    </div>
                                                   
                                            
                                    </div>
                                    <div class="col-md-6">

                                            <label for="purpose" class="form-group"><strong>Details</strong></label>
                                    <textarea type="text" class="form-control" name="details" value="{{ old('details') }}"  readonly>{{$artjob->details}}</textarea>
                                            {!! $errors->first('details','<span class="help-block" style="color:red;">*:message</span>') !!}<br>
                                            <div class="row">
                                            <div class="col-md-6">
                                            <label for="purpose" class="form-group"><strong>Size</strong></label>
                                            <label class="form-control" readonly>{{$artjob->size}}</label>
                                            {!! $errors->first('size','<span class="help-block" style="color:red;">*:message</span>') !!}<br>
                                            </div>
                                             <div class="col-md-6">
                                            <label for="purpose" class="form-group"><strong>Quantity</strong></label>
                                            <label class="form-control" readonly>{{$artjob->quantity}}</label>
                                            {!! $errors->first('quantity','<span class="help-block" style="color:red;">*:message</span>') !!}<br>
                                            </div>
                                            </div>

                                          
                                    </div>
                                </div>

                                {{-- <div class="row">
                                    <div class="col-md-6">asd</div>
                                    <div class="col-md-6">asd</div>
                                </div> --}}
                                @auth


                                @if ($artjob->comment != "")
                                Previous Comment/s:
                                <div class="alert alert-warning" role="alert">
                                    {{$artjob->comment}}
                                  </div>
                                @endif
        
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Comments</label><br>
                                        <form action="/addCommentArt" method="post">
                                            @csrf
                                        <input type="hidden" name="id" value="{{$artjob->id}}">   
                                        <textarea class="form-control" name="comment" value="">{{$artjob->comment}}</textarea><br>
                                        @if ($artjob->comment != "")
                                            <button type="submit" class="btn btn-success" style="float:right;">Update Comment</button>
                                        @else
                                            <button type="submit" class="btn btn-success" style="float:right;">Add Comment</button>
        
                                        @endif
                                        </form>
                                        {!! $errors->first('comment','<span class="help-block" style="color:red;">*:message</span>') !!}    
                                    </div>
                                    </div>
                                   
                                @endauth
                                

                </div>
@endsection
