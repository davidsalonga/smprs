@extends('layouts.app')
@section('title', 'Create Art Job Request')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card animated fadeIn" >
                <div class="card-header">Add Art Job Request Form
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            
                        </div>
                    @endif
                        
                   {!!Form::open(['action' => 'ArtController@store', 'method' => 'POST'])!!}
                            {{ csrf_field() }}
                            
                            <div class="row">
                                    <div class="col-md-4">
                                            <label for="branch" class="form-group"><strong>Branch</strong></label>
                                            <select name="branch" class="form-control" placeholder="Select Branch" value="{{old('branch')}}">
                                                <option value="SMX MANILA">SMX MANILA</option>
                                                <option value="SMX AURA">SMX AURA</option>
                                                <option value="SMX DAVAO">SMX DAVAO</option>
                                                <option value="SMX BACOLOD">SMX BACOLOD</option>
                                                <option value="MEGATRADE HALL">MEGATRADE HALL</option>
                                                <option value="CEBU TRADE HALL">CEBU TRADE HALL</option>
                                                <option value="SKY HALL SEASIDE CEBU">SKY HALL SEASIDE CEBU</option>
                                            </select>
                                            {{-- <input type="text" name="branch" id="" class="form-control" value="{{ old('branch') }}"> --}}
                                            {!! $errors->first('branch','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                    <label for="date_request" class="form-group"><strong>Date Requested</strong></label>
                            <input type="text" class="form-control" name="date_request" 
                            value="{{\Carbon\Carbon::parse($timestamp = now())->format('m/d/Y')}}" readonly>
                                    {!! $errors->first('date_request','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-4">
                                      <label for="date_request" class="form-group"><strong>Requested By</strong></label>
                                      <input type="text" class="form-control" name="request_by" value="{{ old('request_by') }}" >
                                      {!! $errors->first('request_by','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4">
                                <label for="mobileno" class="form-group"><strong>Mobile No. (11 Digits)</strong></label>
                                <input type="text" class="form-control" name="mobileno" value="{{ old('mobileno') }}" placeholder="09xxxxxxxxx" >
                                {!! $errors->first('mobileno','<span class="help-block" style="color:red;">*:message</span>') !!}
                             
                            </div>
                            <div class="col-md-4">
                                    <label for="date_required" class="form-group"><strong>Date Required</strong></label>
                                    
                                    <input type="text" class="form-control datetimepicker" name="date_required"  value="{{ old('date_required') }}" placeholder="mm / dd / yyyy" >
                                    {!! $errors->first('date_required','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            <br><br>
                            <br><br>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <label for="project_name" class="form-group"><strong>Project Name</strong></label>
                                  <input type="text" class="form-control" name="project_name" value="{{ old('project_name') }}" >
                                  {!! $errors->first('project_name','<span class="help-block" style="color:red;">*:message</span>') !!}
                                </div>
                            <div class="col-md-6">
                                    <label for="department" class="form-group"><strong>Department / Designation</strong></label>
                                    <input type="text" class="form-control" name="department"  value="{{ old('department') }}"  >
                                    {!! $errors->first('department','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                           </div>
                           <br>
                           <div class="row">
                                <div class="col-md-6">
                                  <label for="purpose" class="form-group"><strong>Purpose</strong></label>
                                  <textarea type="text" class="form-control" name="purpose" value="{{ old('purpose') }}" ></textarea>
                                  {!! $errors->first('purpose','<span class="help-block" style="color:red;">*:message</span>') !!}
                                </div>
                            <div class="col-md-6">
                                    <label for="usage" class="form-group"><strong>Usage</strong></label>
                                    <textarea type="text" class="form-control" name="usage"  value="{{ old('usage') }}"  ></textarea>
                                    {!! $errors->first('usage','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                           </div>
                            <br><hr>
                            <div class="row">
                                <div class="col-md-6">
                                        <label for="eventDetails" class="form-group"><strong>REQUIREMENTS</strong> </label>
                                        
                                        {!! $errors->first('requirements','<span class="help-block" style="color:red;">*:message</span>') !!}
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                                <div class="checkbox">
                                                    <label for="requirements[]-0">
                                                      Artwork
                                                    </label>
                                                    </div>
                                                    <div class="checkbox">
                                                                  <input type="checkbox" name="requirements[]" id="requirements[]-0" value="Artwork - New"> New  &nbsp; 
                                                                  <input type="checkbox" name="requirements[]" id="requirements[]-0" value="Artwork - Existing"> Existing
                                                    </div>
                                                    <hr>
                                                </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-1">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-1" value="Billboard">
                                                      Billboard
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-2">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-2" value="Poster">
                                                      Poster
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-3">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-3" value="Ad">
                                                      Ad
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-4">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-4" value="Brochure / Fact Sheet">
                                                      Brochure / Fact Sheet
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-5">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-5" value="Flyer">
                                                      Flyer
                                                    </label>
                                                    </div>
                                                    <hr>
                                                    <label for="eventDetails" class="form-group"><strong>Paper</strong> </label>
                                                    <div class="checkbox">
                                                            <label for="requirements[]-11">
                                                              <input type="checkbox" name="requirements[]" id="requirements[]-11" value="A4">
                                                              A4
                                                            </label>
                                                            </div>
                                                            <div class="checkbox">
                                                                    <label for="requirements[]-12">
                                                                      <input type="checkbox" name="requirements[]" id="requirements[]-12" value="A3">
                                                                      A3
                                                                    </label>
                                                                    </div>
                                            
                                    </div>
                                    
                                    <div class="col-md-3">
                                            <div class="checkbox">
                                                    <label for="requirements[]-6">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-6" value="Printing">
                                                      Printing
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-7">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-7" value="Business Card">
                                                      Business Card
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-8">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-8" value="Letterhead / Envelop">
                                                      Letterhead / Envelop
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-9">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-9" value="LED / LFD">
                                                      LED / LFD
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-10">
                                                      <input type="checkbox" name="requirements[]" id="requirements[]-10" value=" Social Media">
                                                      Social Media
                                                    </label>
                                                    </div>
                                                  <div class="checkbox">
                                                    <label for="requirements[]-11">
                                                      Others 
                                                      <textarea class="form-control" name="others" value="{{ old('others') }}" placeholder="Please specify" ></textarea>
                                                    </label>
                                                    </div>
                                                   
                                            
                                    </div>
                                    <div class="col-md-6">

                                            <label for="purpose" class="form-group"><strong>Details</strong></label>
                                            <textarea type="text" class="form-control" name="details" value="{{ old('details') }}" ></textarea>
                                            {!! $errors->first('details','<span class="help-block" style="color:red;">*:message</span>') !!}<br>
                                            <div class="row">
                                            <div class="col-md-6">
                                            <label for="purpose" class="form-group"><strong>Size</strong></label>
                                            <input type="text" class="form-control" name="size" value="{{ old('size') }}" >
                                            {!! $errors->first('size','<span class="help-block" style="color:red;">*:message</span>') !!}<br>
                                            </div>
                                             <div class="col-md-6">
                                            <label for="purpose" class="form-group"><strong>Quantity</strong></label>
                                            <input type="text" class="form-control" name="quantity" value="{{ old('quantity') }}" >
                                            {!! $errors->first('quantity','<span class="help-block" style="color:red;">*:message</span>') !!}<br>
                                            </div>
                                            </div>

                                          
                                    </div>
                                </div>

                                {{-- <div class="row">
                                    <div class="col-md-6">asd</div>
                                    <div class="col-md-6">asd</div>
                                </div> --}}
                                
                             <div class="row">
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-default" style="float:right;">Submit</button>
                                 </div>
                             </div>
                      
                        {!!Form::close()!!}

                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
        <label class="col-md-4 control-label" for="requirements[]"></label>
        <div class="col-md-4">
       
        </div>
      </div>
@endsection