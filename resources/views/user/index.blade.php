@extends('layouts.app')
@section('title', 'View Art Job Request')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card animated fadeIn">
                    <div class="card-header">Manage Users
                            <a href="users/create" class="btn btn-primary" style="float:right; color:white;" >+ Add User</a>
                    </div>
    
                    <div class="card-body">
                        <div class="container">
                            <div class="row col-md-12">
                            <table class="table table-striped ">
                            <thead>
                                   
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                                    @foreach ($users as $user)
                                             <tr>
                                             <td>{{$user->id}}</td>
                                             <td>{{$user->name}}</td>
                                             <td>{{$user->email}}</td>
                                             <td class="text-center">       
                                                   
                                                @if($user->name == 'Admin')
                                                    {!!Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE'])!!}
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit" disabled>Delete</button>
                                                    {!!Form::close()!!}
                                                @else
                                                    {!!Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE'])!!}
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                    {!!Form::close()!!}
                                                @endif
                                            </tr>
                                    @endforeach
                                       
                                    
                            </table>
                            {{$users->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection