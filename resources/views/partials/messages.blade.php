
@if (session('success') )
    <div class="alert alert-success animated fadeIn" >
        <center>{{session('success')}}<br>
            COPY Reference #:  <font style="color:red;"> <strong>{{session('reference')}}</strong></font><br>
          </center>
</div>
@endif

@if (session('successDelete') )
    <div class="alert alert-success animated fadeIn" >
        <center>{{session('success')}}
          <strong>{{session('successDelete')}}</strong></font><br>
          </center>
</div>
@endif

@if (session('proposalUpdated'))
    <div class="alert alert-success animated fadeIn">
        <center>{{session('proposalUpdated')}}<br></center>
</div>
@endif

@if (session('Updatesuccess'))
    <div class="alert alert-success animated fadeIn">
        <center>{{session('Updatesuccess')}}<br>
</div>
@endif

@if (session('commentAdded'))
    <div class="alert alert-success animated fadeIn">
        <center>{{session('commentAdded')}}<br>             
</div>
@endif

@if (session('error'))
    <div class="alert alert-danger animated animated fadeIn">
       <center> {{session('error')}} </center>
    </div>
@endif