<div style="float:right;">
    <a href="/artjob/" class="btn btn-secondary">
    All
    </a>
     &nbsp;
    <a href="/artjob/revised" class="btn btn-primary">
            Revised <span class="badge badge-light">{{$revised}}</span>
    </a>
    &nbsp;
    <a href="/artjob/forRevision" class="btn btn-warning">
            For Revision <span class="badge badge-light">{{$forRevision}}</span>
    </a>
    &nbsp;
    <a href="/artjob/pending" class="btn btn-danger">
            Pending <span class="badge badge-light">{{$pending}}</span>
    </a>&nbsp;
    <a href="/artjob/approved" class="btn btn-success">
            Approved <span class="badge badge-light">{{$approved}}</span>
    </a>&nbsp;
    <a href="/exportArt" >
      <span class="badge"><img src="{{ asset('images/excell.png') }}" height="30px" width="30px"/></span>
    </a>
</div>