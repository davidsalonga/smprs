@extends('layouts.app')
@section('title', 'View Art Job')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card animated fadeIn">
                    <div class="card-header">Art Job Requests / For Revision
                            @include('status.separator2')
                    </div>
    
                    <div class="card-body">
                        @if (count($artjob) > 0)
                        <table class="table table-hover">
                                <thead>
                                 <tr>
                                   <th scope="col">Title</th>
                                   <th scope="col">Requested By</th>
                                   <th scope="col">Date Submitted</th>
                                   <th scope="col">Status</th>
                                 </tr>
                               </thead>
                               <tbody>
                                @foreach ($artjob as $artjobs)
                                         <tr>
                                           <td><u><a href="/artjob/{{$artjobs->id}}"> {{$artjobs->project_name}}</a></u></td>
                                           <td>{{$artjobs->request_by}}</td>
                                           <td>{{ \Carbon\Carbon::parse($artjobs->created_at)->format('m/d/Y - h:i a')}}</td>
                                           <td>
                                                   @if ($artjobs->status == "2")
                                                   <font color="#f9a825"><strong>For Revision - {{ \Carbon\Carbon::parse($artjobs->updated_at)->format('m/d/Y - h:i a')}}</strong></font>
                                                   @elseif($artjobs->status == "3")       
                                                   <font color="#f9a825"><strong>Revised - {{ \Carbon\Carbon::parse($artjobs->updated_at)->format('m/d/Y - h:i a')}}</strong></font>
                                                   @else
                                                   <font color="RED"><strong>PENDING</strong></font>
                                                   @endif
                                            </td>
                                         </tr>              
                                @endforeach
                               </tbody>
                               </table>
                        @else
                            <p>No Proposals Found</p>     
                        @endif        
                        {{$artjob->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection