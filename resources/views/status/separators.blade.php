<div style="float:right;">
        <a href="/proposals/" class="btn btn-secondary">
        All
        </a>
         &nbsp;
        <a href="/proposals/revised" class="btn btn-primary">
                Revised <span class="badge badge-light">{{$revised}}</span>
        </a>
        &nbsp;
        <a href="/proposals/forRevision" class="btn btn-warning">
                For Revision <span class="badge badge-light">{{$forRevision}}</span>
        </a>
        &nbsp;
        <a href="/proposals/pending" class="btn btn-danger">
                Pending <span class="badge badge-light">{{$pending}}</span>
        </a>&nbsp;
        <a href="/proposals/approved" class="btn btn-success">
                Approved <span class="badge badge-light">{{$approved}}</span>
        </a>&nbsp;
        <a href="/export" >
          <span class="badge"><img src="{{ asset('images/excell.png') }}" height="30px" width="30px"/></span>
        </a>
 </div>