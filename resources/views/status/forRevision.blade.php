@extends('layouts.app')
@section('title', 'View Proposal')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card animated fadeIn">
                    <div class="card-header">Pending Requests / For Revision
                            @include('status.separators')
                    </div>
    
                    <div class="card-body">
                        @if (count($proposals) > 0)
                        <table class="table table-hover">
                                <thead>
                                 <tr>
                                   <th scope="col">Title</th>
                                   <th scope="col">Requested By</th>
                                   <th scope="col">Date Submitted</th>
                                   <th scope="col">Status</th>
                                 </tr>
                               </thead>
                               <tbody>
                                @foreach ($proposals as $proposal)
                                         <tr>
                                           <td><u><a href="/proposals/{{$proposal->id}}"> {{$proposal->eventTitle}}</a></u></td>
                                           <td>{{$proposal->request_by}}</td>
                                           <td>{{ \Carbon\Carbon::parse($proposal->created_at)->format('m/d/Y - h:i a')}}</td>
                                           <td>
                                                   @if ($proposal->status == "2")
                                                   <font color="#f9a825"><strong>For Revision - {{ \Carbon\Carbon::parse($proposal->updated_at)->format('m/d/Y - h:i a')}}</strong></font>
                                                   @elseif($proposal->status == "3")       
                                                   <font color="#f9a825"><strong>Revised - {{ \Carbon\Carbon::parse($proposal->updated_at)->format('m/d/Y - h:i a')}}</strong></font>
                                                   @else
                                                   <font color="RED"><strong>PENDING</strong></font>
                                                   @endif
                                            </td>
                                         </tr>              
                                @endforeach
                               </tbody>
                               </table>
                        @else
                            <p>No Proposals Found</p>     
                        @endif        
                        {{$proposals->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection