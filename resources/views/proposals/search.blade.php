@extends('layouts.app')
@section('title', 'Search Requests')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card animated fadeIn">
                <div class="card-header">Search Request</div>

                <div class="card-body">
                <br> <br>
                <div class="row">
                <div class="col-md-2"></div>  
                <div class="col-md-8">
                    <label class="form-group">Enter Reference # (<small>eg. 2019-01-16 09:43:51</small>)</label>
                    <form  action="/proposals/searched" method="get">
                        {{ csrf_field() }}
                        <input type="text"  name="searchPost" class="form-control"/><br>
                        {!! $errors->first('searchPost','<span class="help-block" style="color:red;">*:message</span>') !!}
                        <br>
                        <button type="submit" class="btn btn-default" style="float:right;">Search</button>
                    </form>
                  </div>  
                    <div class="col-md-2"></div>  
               </div>
                </div>
               <br><br>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
