@extends('layouts.app')
@section('title', 'View Social Media Request')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card animated fadeIn">
                <div class="card-header">Viewing Request  : <font color="red"><strong>{{$proposal->created_at}}</strong></font>
                    <font style="float:right;"> Status:

                        @auth
                        @if ($proposal->status == "1") 
                             <font color="green"><strong>APPROVED</strong></font> 
                        @elseif($proposal->status == "2")
                            <a href="/123" id="test" ><font color="#f9a825"><strong>For Revision</strong></font><br></a>
                                <form action="/approve" method="post">
                                    @csrf
                                        <input type="hidden" name="proposalId" value="{{$proposal->id}}">
                                        <button type="submit" class="btn btn-success" style="float:right;">Approve</button>
                                </form>
                         @elseif($proposal->status == "3")
                             <font color="#f9a825"><strong>Revised - to be checked</strong></font><br>
                                <form action="/approve" method="post">
                                    @csrf
                                      <input type="hidden" name="proposalId" value="{{$proposal->id}}">
                                        <button type="submit" class="btn btn-success"  style="float:right;">Approve</button>
                                </form>

                          @else($proposal->status == "0")
                                <font color="red"><strong>PENDING</strong></font>

                                  <form action="/approve" method="post">
                                     @csrf
                                         <input type="hidden" name="proposalId" value="{{$proposal->id}}">
                                         <button type="submit" class="btn btn-success"  style="float:right;">Approve</button>
                                  </form>
                        @endif    
                        @endauth
                        
                        @guest
                        @if ($proposal->status == "1") 
                           <font color="green"><strong>APPROVED</strong></font> 
                        @elseif($proposal->status == "2")
                        <font color="#f9a825"><strong>For Revision</strong></font><br>
                        @elseif($proposal->status == "3")
                        <font color="#f9a825"><strong>Revised - to be checked</strong></font><br>
                        @else
                        <font color="red"><strong>PENDING</strong></font>
                        @endif
                        <br>
                        <a href="/proposals/{{$proposal->id}}/edit" class="btn btn-default" style="float:right;">Edit Form</a> 
                       @endguest    
                        
                    </font>
                </div>

                <div class="card-body">

                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            
                        </div>
                       @endif
                        
                        
                        <form action="" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                    <div class="col-md-4">
                                        <label for="date_request" class="form-group"><strong>Branch </strong></label><br>
                                        <label for="date_request" class="form-group">{{$proposal->branch}}</label>
                                    </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                
                                    <label for="date_request" class="form-group"><strong>Date Requested </strong></label><br>
                                <label for="date_request" class="form-group">{{$proposal->date_request}}</label>
                            </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-4">
                                      <label for="date_request" class="form-group"><strong>Requested By: </strong></label><br>
                                      <label for="date_request" class="form-group">{{$proposal->request_by}}</label>
                                    </div>
                            <div class="col-md-4">
                                <label for="date_request" class="form-group"><strong>Mobile No. </strong></label><br>
                                <label for="date_request" class="form-group">{{$proposal->mobileno}}</label>
                            </div>
                            <div class="col-md-4">
                                <label for="date_request" class="form-group"><strong>Schedule of Post </strong></label><br>
                                <label for="date_request" class="form-group">{{$proposal->schedule_post}}</label>
                            </div>
                            </div>
                            <br><hr>
                            <div class="row">
                                <div class="col-md-6">
                                        <br>
                                        
                                        <label for="eventDetails" class="form-group"><strong>Event Details:</strong> </label>&nbsp;&nbsp;&nbsp;
                                        @if($array = explode(",", $proposal->details))
                                             
                                        @foreach ($array as $item)
                                        &nbsp;<input type="checkbox" {{strpos($proposal->details, $item) !== false ? "checked" : ""}} disabled/>&nbsp;&nbsp;{{$item}}
                                        @endforeach
                                        
                                        @endif
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-3">
                                            <label for="eventTitle"><strong>Event Title</strong></label><br>
                                            <label for="date_request" class="form-group">{{$proposal->eventTitle}}</label> 
                                        </div>
                                    <div class="col-md-3">
                                            <label for="eventVenue"><strong>Event Venue</strong></label><br>
                                            <label for="date_request" class="form-group">{{$proposal->eventVenue}}</label>
                                           </div>
                                           <div class="col-md-2">
                                                <label for="eventDate"><strong>Event Date</strong></label><br>
                                                <label for="date_request" class="form-group">{{$proposal->eventDate}}</label>
                                            </div>
                                        <div class="col-md-2">
                                                <label for="eventTime"><strong>Start Time</strong></label><br>
                                                <label for="date_request" class="form-group">{{$proposal->startTime}}</label>
                                           </div> 
                                        <div class="col-md-2">
                                            <label for="eventTime"><strong>End Time</strong></label><br>
                                            <label for="date_request" class="form-group">{{$proposal->endTime}}</label>
                                        </div>
                             </div>
                             <br>
                             <div class="row">
                                <div class="col-md-12">
                                        <label for="socialMediaChannel" class="form-group"><strong>Social Media Channel:</strong></label>&nbsp;&nbsp;&nbsp;
                                        @if($array = explode(",", $proposal->social))
                                             
                                        @foreach ($array as $item)
                                        &nbsp;<input type="checkbox" {{strpos($proposal->social, $item) !== false ? "checked" : ""}} disabled/>&nbsp;&nbsp;{{$item}}
                                        @endforeach
                                        
                                        @endif
                                       
                                </div>
                             </div>

                             <div class="row">
                                 <div class="col-md-12">
                                     <label for="postText" class="form-group"><strong>Post Text/ Caption</strong> *(Include 
                                    important details such as ticket price, website, and email/contact details)</label>
                                    <label for="date_request" class="form-group">{{$proposal->postText}}</label></div>
                             </div>
                             
                             <br>
                            
                             <div class="row">
                                 <div class="col-md-12">
                                     <table class="table table-bordered">
                                         <tr>
                                            <th>Social Media Channel</th>
                                            <th>Image</th>
                                            <th>Video</th>
                                         </tr>
                                         <tr>
                                             <td>Facebook</td>
                                             <td>1200(w)x1800(h)pixels</td>
                                             <td>Video ratio 9:16 to 16:9 / Length: min 1sec-max:240mins. / Size: 4GB max</td>
                                         </tr>
                                         <tr>
                                             <td>Instagram</td>
                                             <td>1080(w)x1350(h) pixels</td>
                                             <td>Video ration: 1:91:1 / Length: 60 seconds / Size: 4GB max</td>
                                         </tr>
                                         <tr>
                                             <td>LinkedIn</td>
                                             <td>1200(w)x628(h) pixels</td>
                                             <td>Video ration: 4:3 / Length min. 3 secs-max: 30mins. / Size: 75kb-200mb</td>
                                         </tr>
                                     </table>
                                 </div>
                             </div>

                             <div class="row">
                                <div class="col-md-12">
                                    <label for="fanpage" class="form-group"><strong>Image/Video URL</strong></label>
                            
                                       
                                    @if($array = explode("\n", $proposal->imgvidUrl))
                                             
                                    @foreach ($array as $item)
                                    
                                    &nbsp;<br><a href="http://{{$item}}"  target="_blank"> {{$item}} </a>
                                   
                                    @endforeach
                                    @endif 
                                    
                                    
                                    
                                </div>
                             </div>
                             <br>
                             <div class="row">
                                 <div class="col-md-6">
                                     <label for="fanpage" class="form-group"><strong>Facebook/Instagram Fan Page</strong>
                                         <br>(Link to your social sites to be tagged)
                                     </label>

                                         
                                    @if($array = explode("\n", $proposal->fanpage))
                                             
                                    @foreach ($array as $item)
                                    
                                    &nbsp;<br><a href="http://{{$item}}"  target="_blank"> {{$item}} </a>
                                   
                                    @endforeach
                                    @endif 

                                     </div>
                                 <div class="col-md-6"></div>
                             </div>
                            <br>
                        </form>

                        @auth


                        @if ($proposal->comment != "")
                        Previous Comment/s:
                        <div class="alert alert-warning" role="alert">
                            {{$proposal->comment}}
                          </div>
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                <label>Comments</label><br>
                                <form action="/addComment" method="post">
                                    @csrf
                                <input type="hidden" name="id" value="{{$proposal->id}}">   
                                <textarea class="form-control" name="comment" value="">{{$proposal->comment}}</textarea><br>
                                @if ($proposal->comment != "")
                                    <button type="submit" class="btn btn-success" style="float:right;">Update Comment</button>
                                @else
                                    <button type="submit" class="btn btn-success" style="float:right;">Add Comment</button>

                                @endif
                                </form>
                                {!! $errors->first('comment','<span class="help-block" style="color:red;">*:message</span>') !!}    
                            </div>
                            </div>
                           
                        @endauth
                        
                        

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
