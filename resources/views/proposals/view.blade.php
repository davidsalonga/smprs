@extends('layouts.app')
@section('title', 'Searched Social Media Request')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card animated fadeIn">
                    <div class="card-header">Pending Requests 
                      

                    </div>
    
                    <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                                
                            </div>
                        @endif
                        @if (count($proposals) > 0 || count($artjob) > 0)
                         @foreach ($proposals as $proposal)
                             <div class="well">
                                 <h3><u><a href="/proposals/{{$proposal->id}}"> {{$proposal->eventTitle}}</a></u></h3>
                                 <small>Date Submitted: {{ \Carbon\Carbon::parse($proposal->created_at)->format('m/d/Y - h:i:s a')}}</small><br>
                                 <small>Requested by: {{$proposal->request_by}}</small><br>
                                 <small>Status: 
                                     {{-- 
                                        
                                     // 1 - Posted
                                     // 2 - For Revision
                                     // 3 - User Updated
                                     // 4 - Pending

                                        --}}
                                   @if ($proposal->status == "1") 
                                     <font color="green"><strong>Posted - {{ \Carbon\Carbon::parse($proposal->updated_at)->format('m/d/Y - h:i a')}}</strong></font> 
                                    @elseif($proposal->status == "2")
                                     <font color="#f9a825"><strong>For Revision - {{ \Carbon\Carbon::parse($proposal->updated_at)->format('m/d/Y - h:i a')}}</strong></font> 
                                     @elseif($proposal->status == "3")
                                     <font color="#f9a825"><strong>Revised - {{ \Carbon\Carbon::parse($proposal->updated_at)->format('m/d/Y - h:i a')}}</strong></font> 
                                    @else
                                     <font color="RED"><strong>PENDING</strong></font> 
                                  @endif
                                    
                                </small>
                                <br><br>
                                @if ($proposal->comment != "")
                                Comment/s:
                                <div class="alert alert-warning" role="alert">
                                    {{$proposal->comment}}
                                  </div>
                                @endif
                                
                                <hr>
                             </div>
                         @endforeach
                         @foreach ($artjob as $artjob)
                             <div class="well">
                                 <h3><u><a href="/artjob/{{$artjob->id}}"> {{$artjob->project_name}}</a></u></h3>
                                 <small>Date Submitted: {{ \Carbon\Carbon::parse($artjob->created_at)->format('m/d/Y - h:i:s a')}}</small><br>
                                 <small>Requested by: {{$artjob->request_by}}</small><br>
                                 <small>Status: 
                                     {{-- 
                                        
                                     // 1 - Posted
                                     // 2 - For Revision
                                     // 3 - User Updated
                                     // 4 - Pending

                                        --}}
                                   @if ($artjob->status == "1") 
                                     <font color="green"><strong>Posted - {{ \Carbon\Carbon::parse($artjob->updated_at)->format('m/d/Y - h:i a')}}</strong></font> 
                                    @elseif($artjob->status == "2")
                                     <font color="#f9a825"><strong>For Revision - {{ \Carbon\Carbon::parse($artjob->updated_at)->format('m/d/Y - h:i a')}}</strong></font> 
                                     @elseif($artjob->status == "3")
                                     <font color="#f9a825"><strong>Revised - {{ \Carbon\Carbon::parse($artjob->updated_at)->format('m/d/Y - h:i a')}}</strong></font> 
                                    @else
                                     <font color="RED"><strong>PENDING</strong></font> 
                                  @endif
                                    
                                </small>
                                <br><br>
                                @if ($artjob->comment != "")
                                Comment/s:
                                <div class="alert alert-warning" role="alert">
                                    {{$artjob->comment}}
                                  </div>
                                @endif
                                
                                <hr>
                             </div>
                         @endforeach

                        @else
                            <p>No Proposals Found</p>     
                        @endif        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection