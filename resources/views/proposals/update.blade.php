@extends('layouts.app')
@section('content')
@section('title', 'Update Proposal')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card animated fadeIn"  style="animation-delay: .25s;">
                <div class="card-header">Update Request Form
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            
                        </div>
                    @endif
                       
                   {!!Form::open(['action' => ['ProposalController@update', $proposal->id], 'method' => 'POST'])!!}
                            {{ csrf_field() }}
                            
                            <div class="row">
                                    <div class="col-md-4">
                                            <label for="branch" class="form-group"><strong>Branch</strong></label>
                                            <select name="branch" class="form-control" value="{{old('branch')}}" placeholder="Select Branch">
                                                    <option value="SMX MANILA">SMX MANILA</option>
                                                    <option value="SMX AURA">SMX AURA</option>
                                                    <option value="SMX DAVAO">SMX DAVAO</option>
                                                    <option value="SMX BACOLOD">SMX BACOLOD</option>
                                                    <option value="MEGATRADE HALL">MEGATRADE HALL</option>
                                                    <option value="CEBU TRADE HALL">CEBU TRADE HALL</option>
                                                    <option value="SKY HALL SEASIDE CEBU">SKY HALL SEASIDE CEBU</option>
                                                </select>
                                            {{-- <input type="text" name="branch" id="" class="form-control" value="{{$proposal->branch}}"> --}}
                                            {!! $errors->first('branch','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                    <label for="date_request" class="form-group"><strong> Date Requested</strong></label>
                            <input type="text" class="form-control" name="date_request" 
                            value="{{\Carbon\Carbon::parse($proposal->created_at)->format('m/d/Y')}}" readonly>
                                    {!! $errors->first('date_request','<span class="help-block " style="color:red;">*:message</span>') !!}
                            </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-4">
                                      <label for="date_request" class="form-group"><strong>Requested By</strong></label>
                                    <input type="text" class="form-control" name="request_by" value="{{$proposal->request_by}}">
                                      {!! $errors->first('request_by','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4">
                                <label for="date_request" class="form-group"><strong>Mobile No.</strong></label>
                                <input type="text" class="form-control" name="mobileno" value="{{ $proposal->mobileno}}" >
                                {!! $errors->first('mobileno','<span class="help-block" style="color:red;">*:message</span>') !!}
                             
                            </div>
                            <div class="col-md-4">
                                    <label for="schedule_post" class="form-group"><strong>Schedule of Post</strong></label>
                                    <input type="text" class="form-control datetimepicker" name="schedule_post" value="{{$proposal->schedule_post}}">
                                    {!! $errors->first('schedule_post','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            </div>
                            <br><hr>
                            <div class="row">
                                <div class="col-md-">
                                        <label for="eventDetails" class="form-group"><strong>Event Details:</strong> </label>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="details[]" id="" value="BDG Event" class=""> BDG Event&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="details[]" id="" value="Client Event" class=""> Client Event
                                     </div>
                                <div class="col-md- animated rollIn">&nbsp; {!! $errors->first('details','<span class="help-block " style="color:red;">*:message</span>') !!} </div>
                                
                            </div>

                            <div class="row">
                                    <div class="col-md-3">
                                            <label for="eventTitle"><strong>Event Title</strong></label>
                                            <input type="text" name="eventTitle" class="form-control" id="" value="{{$proposal->eventTitle}}">    
                                            {!! $errors->first('eventTitle','<span class="help-block animated wobble" style="color:red;">*:message</span>') !!}        
                                    </div>
                                    <div class="col-md-3">
                                            <label for="eventVenue"><strong>Event Venue</strong></label>
                                    <input type="text" name="eventVenue" class="form-control" id="" value="{{$proposal->eventVenue}}">   
                                            {!! $errors->first('eventVenue','<span class="help-block" style="color:red;">*:message</span>') !!}           
                                    </div>
                                    <div class="col-md-2">
                                            <label for="eventDate"><strong>Event Date</strong></label>
                                            <input type="text" name="eventDate" class="form-control datetimepicker2" id=""  value="{{$proposal->eventDate}}"  placeholder="mm / dd / yyyy" >   
                                            {!! $errors->first('eventDate','<span class="help-block" style="color:red;">*:message</span>') !!}           
                                    </div>
                                    <div class="col-md-2">
                                            <label for="eventTime"><strong>Start Time</strong></label>
                                            <input type="text" name="startTime" class="form-control timepicker" id="" value="{{ $proposal->startTime }}"  placeholder="-- : -- --" >    
                                            {!! $errors->first('startTime','<span class="help-block" style="color:red;">*:message</span>') !!}          
                                    </div>
                                    <div class="col-md-2">
                                        <label for="eventTime"><strong>End Time</strong></label>
                                        <input type="text" name="endTime" class="form-control timepicker" id="" value="{{ $proposal->endTime }}"  placeholder="-- : -- --" >    
                                        {!! $errors->first('endTime','<span class="help-block" style="color:red;">*:message</span>') !!}          
                                </div>
                             </div>
                             <br>
                             <div class="row">
                                <div class="col-md-">
                                        <label for="socialMediaChannel" class="form-group"><strong>Social Media Channel:</strong></label>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="social[]" id="" value="Facebook" class=""> Facebook&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="social[]" id="" value="LinkedIn" class=""> LinkedIn(Corporate Events, Seminars , and Job Fairs Only) &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="social[]" id="" value="Instagram" class=""> Instagram
                                        </div>
                                <div class="col-md- animated rollIn" >&nbsp; {!! $errors->first('social','<span class="help-block " style="color:red;">*:message</span>') !!} </div>
                                
                            
                             </div>

                             <div class="row">
                                 <div class="col-md-12">
                                     <label for="postText" class="form-group"><strong>Post Text/ Caption</strong> *(Include 
                                    important details such as ticket price, website, and email/contact details)</label>
                                     <textarea name="postText" id="" cols="30" rows="10" class="form-control">{{$proposal->postText}}</textarea>
                                     {!! $errors->first('postText','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                 </div>
                             </div>
                             <br>
                             <div class="row">
                                 <div class="col-md-12">
                                     <table class="table table-bordered">
                                         <tr>
                                            <th>Social Media Channel</th>
                                            <th>Image</th>
                                            <th>Video</th>
                                         </tr>
                                         <tr>
                                             <td>Facebook</td>
                                             <td>1200(w)x1800(h)pixels</td>
                                             <td>Video ratio 9:16 to 16:9 / Length: min 1sec-max:240mins. / Size: 4GB max</td>
                                         </tr>
                                         <tr>
                                             <td>Instagram</td>
                                             <td>1080(w)x1350(h) pixels</td>
                                             <td>Video ration: 1:91:1 / Length: 60 seconds / Size: 4GB max</td>
                                         </tr>
                                         <tr>
                                             <td>LinkedIn</td>
                                             <td>1200(w)x628(h) pixels</td>
                                             <td>Video ration: 4:3 / Length min. 3 secs-max: 30mins. / Size: 75kb-200mb</td>
                                         </tr>
                                     </table>
                                 </div>
                             </div>

                             <div class="row">
                                    <div class="col-md-12">
                                        <label for="fanpage" class="form-group"><strong>Image/Video URL</strong> - Url Shortener : <a href="https://bitly.com/" target="_blank">Click Here</a></label>
                                       <textarea name="imgvidUrl" id="" cols="1" rows="1" class="form-control">{{$proposal->imgvidUrl}}</textarea>
                                       {!! $errors->first('imgvidUrl','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                    </div>
                             </div>
                            <br>
                             <div class="row">
                                 <div class="col-md-6">
                                     <label for="fanpage" class="form-group"><strong>Facebook/Instagram Fan Page</strong>
                                         <br>(Link to your social sites to be tagged)
                                     </label>
                                    <textarea name="fanpage" id="" cols="30" rows="10" class="form-control">{{$proposal->fanpage}}</textarea>
                                    {!! $errors->first('fanpage','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                 </div>
                                 <div class="col-md-6"></div>
                             </div>
                             <input type="hidden" name="comment" value="{{$proposal->comment}}">
                            <br>
                            <input type="hidden" name="_method" value="put" />
                            <input type="hidden" name="id" value="{{$proposal->id}}" />
                             <div class="row">
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-default" style="float:right;">Update</button>
                                 </div>
                             </div>
                      
                        {!!Form::close()!!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
