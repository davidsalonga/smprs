@extends('layouts.app')
@section('title', 'View Social Media Request')
@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card animated fadeIn">
                    <div class="card-header">Pending Request 
                            @include('status.separators')
                    </div>
    
                    <div class="card-body">
                        @if (count($proposals) > 0)
                        <table class="table table-hover">
                         <thead>
                          <tr>
                            <th scope="col">Title</th>
                            <th scope="col">Requested By</th>
                            <th scope="col">Date Submitted</th>
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                         @foreach ($proposals as $proposal)
                                  <tr>
                                    <td><u><a href="/proposals/{{$proposal->id}}"> {{$proposal->eventTitle}}</a></u></td>
                                    <td>{{$proposal->request_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($proposal->created_at)->format('m/d/Y - h:i a')}}</td>
                                    <td>
                                            <font color="green"><strong>APPROVED</strong></font>
                                  </tr>              
                         @endforeach
                        </tbody>
                        </table>
                        @else
                            <p>No Proposals Found</p>     
                        @endif        
                        {{$proposals->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection