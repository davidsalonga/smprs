@extends('layouts.app')
@section('title', 'Create Social Media Request')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card animated fadeIn" >
                <div class="card-header">Add Request Form
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                            
                        </div>
                    @endif
                       
                   {!!Form::open(['action' => 'ProposalController@store', 'method' => 'POST'])!!}
                            {{ csrf_field() }}
                            
                            <div class="row">
                                    <div class="col-md-4">
                                            <label for="branch" class="form-group"><strong>Branch</strong></label>
                                            <select name="branch" class="form-control" placeholder="Select Branch" value="{{old('branch')}}">
                                                <option value="SMX MANILA">SMX MANILA</option>
                                                <option value="SMX AURA">SMX AURA</option>
                                                <option value="SMX DAVAO">SMX DAVAO</option>
                                                <option value="SMX BACOLOD">SMX BACOLOD</option>
                                                <option value="MEGATRADE HALL">MEGATRADE HALL</option>
                                                <option value="CEBU TRADE HALL">CEBU TRADE HALL</option>
                                                <option value="SKY HALL SEASIDE CEBU">SKY HALL SEASIDE CEBU</option>
                                            </select>
                                            {{-- <input type="text" name="branch" id="" class="form-control" value="{{ old('branch') }}"> --}}
                                            {!! $errors->first('branch','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                    <label for="date_request" class="form-group"><strong> Date Requested</strong></label>
                            <input type="text" class="form-control" name="date_request" 
                            value="{{\Carbon\Carbon::parse($timestamp = now())->format('m/d/Y')}}" readonly>
                                    {!! $errors->first('date_request','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="col-md-4">
                                      <label for="date_request" class="form-group"><strong>Requested By</strong></label>
                                      <input type="text" class="form-control" name="request_by" value="{{ old('request_by') }}" >
                                      {!! $errors->first('request_by','<span class="help-block" style="color:red;">*:message</span>') !!}
                                    </div>
                            <div class="col-md-4">
                                <label for="date_request" class="form-group"><strong>Mobile No. (11 Digits)</strong></label>
                                <input type="text" class="form-control" name="mobileno" value="{{ old('mobileno') }}" placeholder="09xxxxxxxxx" >
                                {!! $errors->first('mobileno','<span class="help-block" style="color:red;">*:message</span>') !!}
                             
                            </div>
                            <div class="col-md-4">
                                    <label for="schedule_post" class="form-group"><strong>Schedule of Post</strong></label>
                                    
                                    <input type="text" class="form-control datetimepicker" name="schedule_post"  value="{{ old('schedule_post') }}" placeholder="mm / dd / yyyy" >
                                    {!! $errors->first('schedule_post','<span class="help-block" style="color:red;">*:message</span>') !!}
                            </div>
                            </div>
                            <br><hr>
                            <div class="row">
                                <div class="col-md-6">
                                        <label for="eventDetails" class="form-group"><strong>Event Details:</strong> </label>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="details[]" id="" value="BDG Event" class=""> BDG Event&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="details[]" id="" value="Client Event" class=""> Client Event
                                        {!! $errors->first('details','<span class="help-block" style="color:red;">*:message</span>') !!}
                                </div>
                                <div class="col-md-6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Event Proper</strong></div>
                            </div>

                            <div class="row">
                                    <div class="col-md-3">
                                            <label for="eventTitle"><strong>Event Title</strong></label>
                                            <input type="text" name="eventTitle" class="form-control" id=""  value="{{ old('eventTitle') }}">    
                                            {!! $errors->first('eventTitle','<span class="help-block" style="color:red;">*:message</span>') !!}        
                                    </div>
                                    <div class="col-md-3">
                                            <label for="eventVenue"><strong>Event Venue</strong></label>
                                            <input type="text" name="eventVenue" class="form-control" id=""  value="{{ old('eventVenue') }}">   
                                            {!! $errors->first('eventVenue','<span class="help-block" style="color:red;">*:message</span>') !!}           
                                    </div>
                                    <div class="col-md-2">
                                            <label for="eventDate"><strong>Event Date</strong></label>
                                            <input type="text" name="eventDate" class="form-control datetimepicker2" id=""  value="{{ old('eventDate') }}"  placeholder="mm / dd / yyyy" >   
                                            {!! $errors->first('eventDate','<span class="help-block" style="color:red;">*:message</span>') !!}           
                                    </div>
                                    <div class="col-md-2">
                                            <label for="eventTime"><strong>Start Time</strong></label>
                                            <input type="text" name="startTime" class="form-control timepicker" id="" value="{{ old('startTime') }}"  placeholder="-- : -- --" >    
                                            {!! $errors->first('startTime','<span class="help-block" style="color:red;">*:message</span>') !!}          
                                    </div>
                                    <div class="col-md-2">
                                        <label for="eventTime"><strong>End Time</strong></label>
                                        <input type="text" name="endTime" class="form-control timepicker" id="" value="{{ old('endTime') }}"  placeholder="-- : -- --" >    
                                        {!! $errors->first('endTime','<span class="help-block" style="color:red;">*:message</span>') !!}          
                                </div>
                             </div>
                             <br>
                             <div class="row">
                                <div class="col-md-12">
                                        <label for="socialMediaChannel" class="form-group"><strong>Social Media Channel:</strong></label>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="social[]" id="" value="Facebook" class=""> Facebook&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="social[]" id="" value="LinkedIn" class=""> LinkedIn(Corporate Events, Seminars , and Job Fairs Only) &nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="social[]" id="" value="Instagram" class=""> Instagram
                                        {!! $errors->first('social','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                </div>
                             </div>

                             <div class="row">
                                 <div class="col-md-12">
                                     <label for="postText" class="form-group"><strong>Post Text/ Caption</strong> *(Include 
                                    important details such as ticket price, website, and email/contact details)</label>
                                     <textarea name="postText" id="" cols="30" rows="10" class="form-control"> {{ old('postText') }}</textarea>
                                     {!! $errors->first('postText','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                 </div>
                             </div>
                             <br>
                             <div class="row">
                                 <div class="col-md-12">
                                     <table class="table table-bordered">
                                         <tr>
                                            <th>Social Media Channel</th>
                                            <th>Image</th>
                                            <th>Video</th>
                                         </tr>
                                         <tr>
                                             <td>Facebook</td>
                                             <td>1200(w)x1800(h)pixels</td>
                                             <td>Video ratio 9:16 to 16:9 / Length: min 1sec-max:240mins. / Size: 4GB max</td>
                                         </tr>
                                         <tr>
                                             <td>Instagram</td>
                                             <td>1080(w)x1350(h) pixels</td>
                                             <td>Video ration: 1:91:1 / Length: 60 seconds / Size: 4GB max</td>
                                         </tr>
                                         <tr>
                                             <td>LinkedIn</td>
                                             <td>1200(w)x628(h) pixels</td>
                                             <td>Video ration: 4:3 / Length min. 3 secs-max: 30mins. / Size: 75kb-200mb</td>
                                         </tr>
                                     </table>
                                 </div>
                             </div>

                             <div class="row">
                                    <div class="col-md-12">
                                        <label for="fanpage" class="form-group"><strong>Image/Video URL</strong> <br> Url Shortener : <a href="https://bitly.com/" target="_blank">Click Here</a></label>
                                       <textarea name="imgvidUrl" id="" cols="1" rows="1" class="form-control" placeholder="Please paste your link here">{{ old('imgvidUrl') }}</textarea>
                                       {!! $errors->first('imgvidUrl','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                    </div>
                             </div>
                            <br>
                             <div class="row">
                                 <div class="col-md-6">
                                     <label for="fanpage" class="form-group"><strong>Facebook/Instagram Fan Page</strong>
                                         <br>(Link to your social sites to be tagged)
                                     </label>
                                    <textarea name="fanpage" id="" cols="30" rows="10" class="form-control">{{ old('fanpage') }}</textarea>
                                    {!! $errors->first('fanpage','<span class="help-block" style="color:red;">*:message</span>') !!}  
                                 </div>
                                 <div class="col-md-6"></div>
                             </div>
                             <input type="hidden" name="comment" value=" ">
                            <br>
                             <div class="row">
                                 <div class="col-md-12">
                                    <button type="submit" class="btn btn-default" style="float:right;">Submit</button>
                                 </div>
                             </div>
                      
                        {!!Form::close()!!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
